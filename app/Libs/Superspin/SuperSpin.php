<?php namespace App\Libs\Superspin;

use DB;

class SuperSpin {

    private function getAllTemplate(){
        $this->templates = DB::table('spinner')->select('*')->get();
    }

    private function get_option($template)
    {
        $result = ""; 
        foreach($this->templates as $row){
            if($row->spin_type==$template) $result = $row->text;
        }

        return $result;
    }

    /**
    * Shuffle for SEO by Key
    */
    private function superspin_shuffle($items, $key = 'nokey')
    {
        $ord = 0;
        foreach(str_split($key) as $str)
        {
            if(is_int($str))
            {
                $ord += intval($str);
            }
            else
            {
                $ord += intval(ord($str));
            }
        }

        mt_srand($ord); 
        for ($i = count($items) - 1; $i > 0; $i--)
        { 
            $j = @mt_rand(0, $i); 
            $tmp = $items[$i]; 
            $items[$i] = $items[$j]; 
            $items[$j] = $tmp; 
        } 

        return $items;
    }

    private function superspin_phrase($phrase, $key = '') {
        $match = explode('|', $phrase);
        $match =  $this->superspin_shuffle($match, $key);
        return $match[0];
    }


    public function spin_text($post)
    {
        $this->getAllTemplate();
        $this->post = $post;
        $keyphrase = ($post->id > 0) ? $post->id: 0;

        $templateName = ['alpha', 'beta', 'gamma', 'delta', 'epsilon', 'zeta', 'eta', 'theta', 'iota', 'kappa'];
        $template = [];
        foreach($templateName as $key=>$value) {
            $t = stripslashes($this->get_option($value));
            if ($t != '') {
                array_push($template, $t);
            }
        }

        $template = $this->superspin_shuffle($template, 'tpl'. $keyphrase);

        return $this->superspin($template[0], $keyphrase);



    }

    private function superspin($template, $keyphrase = '') 
    {
        $match_syntax = '/\{(.*?)\}/';
        preg_match_all($match_syntax, $template, $matches, PREG_PATTERN_ORDER);
        $results = $matches[0];
        $content = $template;

        if(is_array($results) && count($results) > 0)
        {
            foreach($results as $key => $result)
            {
                $clean_result = str_replace(array('{', '}'), '', $result);
                $random = $this->superspin_phrase($clean_result, 'phrase'. $keyphrase . $key);
                $content = str_replace($result, $random, $content);
            }
        }

        $content = str_replace('&lt;% keyword %&gt;', $this->post->title, $content);

        return $content;
    }


}