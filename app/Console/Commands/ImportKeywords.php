<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;

class ImportKeywords extends Command {

  protected $name = 'content:importkeyword';

  protected $description = 'Import keywords csv to database';

  public function __construct()
  {
    parent::__construct();
  }

  public function fire()
  {
    $this->info("run import keyword command");
    $this->generateData(); 
    $this->info("done..");
  }

  private function sampleData()
  {
     $file = $this->option('file');
     if(!$file) {
        $this->error('file option not defined. please add --file "filename.csv"'); 
        return false;
     }
     $path = base_path() . "/import/". $file; 
     $this->info('load data in ' . $path);
     if(!file_exists($path)){
        $this->error('file not exits, exit..');
        return false;
     }

      $index = 1;
      $data = [];
      if (($handle = fopen($path, "r")) !== FALSE) {
          while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
              if($index){
                $data[] = [
                  'keyword' => $row[0]
                ];
              }
              $index++;
          }
          fclose($handle);
      }

      $this->info('load '. ($index-1) . ' records..'); 

      return $data;

  }

  public function generateData()
  {
    $data = $this->sampleData(); 
    if($data === false) return false;
    //$this->info('shuffle data'); 
    //shuffle($data);
    foreach($data as $row){
      $found = DB::table('keywords')->where('keyword', 'like', $row['keyword'])->first(); 
      if(!$found){
        $this->info('insert keyword '. $row['keyword']);
        $row['created_at'] = DB::raw('now()'); 
        DB::table('keywords')->insert($row);
        $this->info('-----success insert new record------------');        
      }
    }
  }

  protected function getArguments()
  {
    return [
      //['example', InputArgument::REQUIRED, 
        //'An example argument.'],
    ];
  }

  protected function getOptions()
  {
    return [
      ['file', null, InputOption::VALUE_REQUIRED, 
        'file name in folder import.', null],
    ];
  }

}