<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class KeywordController extends Controller {

    public function index($slug, $page=1)
    {


        if(!is_numeric($page)) $page = 1;
        $limit = 12; 
        $start = ($page -1) * $limit;


        $sql = DB::table('posts')
                    ->select('*')
                    ->where('published','1')
                    ->where('slug_keyword', $slug)
                    ->orderBy('title','ASC'); 

        $total = $sql->count(); 

        $data  = $sql->take($limit)->skip($start)->get();
        $keyword = str_replace('-', ' ', $slug);

        $posts = [
            'data' => $data,
            'total' => $total,
            'limit' => $limit
        ];


        $titles = []; 
        foreach($data as $row){
            $titles[] = $row->title;
        }

        $title = ($page > 1)? (' page '. $page): ''; 

        return view('pages.keyword',
            [
             'posts'=> $posts,
             'keyword' => $keyword,
             'slug' => $slug,
             'current_title' =>  $keyword . $title,
             'current_description' => implode('. ', $titles)
             ]
        );
    }

}