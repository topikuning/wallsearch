--- Auto wallpaper site 
--required php 5.5.9 ~

$ cd /rootapp

1. copy/rename .env.example di root folder menjadi .env
   set writeable storage IMPORTANT
   $ chmod -R o+w storage/  
2. buka .env sesuaikan database, set jadi production mode, debug mode false
3. buka config/site.php untuk setting nama site, deksripsi, footer, dll
   site_url (wajib) berpengaruh ke sitemap

3. $ composer install 
4. $ php artisan migrate

--scrapping data (wajib install)
5. install latest nodejs dari https://nodejs.org/
   - jika memakai centos5 baca file install_nodejs_on_centos5
   setelah install
   $ cd nodescrap 
   $ sudo npm install -g forever
   $ forever start app.js
   - tambahkan forever sebagai service saat boot up server
     forever start /path/to/appweb/nodescrap app.js
   
6. tambahkan cronjob 
  $ crontab -e
  tambah dengan * * * * * php /var/www/html/wallpaper-web/artisan schedule:run 1>> /dev/null 2>&1

7. spinner table 
   untuk contoh spinner valid data spinner table run 
   $ php artisan db:seed

8. import contoh csv keywords dengan 
   $ php artisan content:importkeyword --file "keywords.csv" 
   - untuk tambah data keyword sendiri letakkan di folder import/ dan run 
     php artisan content:importkeyword --file "filemu.csv"

9. run manual scrap 
   $ php artisan content:scrap

10. sitemap 
   - cara update manual 
   $ php artisan sitemap:update 

11. cara mereset table didatabase 
   $ php artisan content:resetdb

12. cara melihat command artisan lengkap 
   $ php artisan list 

13. melihat daemon scrap nodejs apakah berjalan apa nggak 
   $ forever list

-- limitation 
   - kategory bukan nested tree. flat kategory

  IMPORTANT!!
  jangan pernah aktifkan cache setting untuk file image di konfigurasi apache/Nginx karena file image bersifat dynamic dari url domain lain.  hanya aktifkan leverage cache server untuk css,js assets 
