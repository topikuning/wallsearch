@extends('home')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="post-container">
                <div class="post-content">
                    <div class="widget-title">
                      <h3>{{ $keyword }} Gallery</h3>
                    </div>
                        <div class="row wrapper-portfolio" itemscope itemtype="http://schema.org/ImageGallery">
                           @include('partials._posts')
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- begin:pagination -->
    <div class="row">
      <div class="col-md-12 text-center">
        <ul class="pagination">
           <?php $page = 1; $start=0; $limit= $posts['limit']; ?>
           @while ($start < $posts['total'])

           <?php
              $url = url('/'.$slug);
              if($page > 1) $url = url('/'.$slug.'/page/'. $page);    
           ?>

           <li><a href="{{ $url }}" title="{{ $keyword }} page {{ $page }}"><i class="fa fa-file-image-o"></i></a></li>
           <?php $start = $start + $limit;  $page++; ?>
           @endwhile
          
        </ul>
      </div>
    </div>
    <!-- end:pagination -->


@endsection