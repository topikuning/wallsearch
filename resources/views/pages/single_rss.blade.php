
<?php
    $url = url('singleimages/'. $post->slug.'-'.$post->code . '.jpg');
?>
<a href="{{ url($post->slug.'/'.$post->code) }}" title="{{ $post->title }}">
    <img width="500" src="{{ $url }}" alt="{{ $post->title }}" />
</a>