@extends('home')
@section('content')

<section class="blog-section">
  <div class="container">
    <div class="row">

      <div class="col-md-12">
        <div class="blog-page-section">
          <div class="blog-area animate fadeInUp" data-anim-type="fadeInUp" data-anim-delay="400">

            <div class="clear"></div>
            <div class="blog-post-title">
              <div class="blog-post-title-wrapper">
                <h1 class="title">{{ $page->title }}</h1>

                <p>{!! str_replace('nama.situs',config('site.site_url'),$page->body) !!}</p>

              </div>
            </div>
          </div>

          <!--comment Section-->
        </div>
      </div>
      <!--Right Sidebar-->
      <div class="col-md-3">
        <div class="sm-right-sidebar animate" data-anim-type="fadeInUp" data-anim-delay="500">
        </div>
      </div>
    </div>
  </div>
</section>

@endsection