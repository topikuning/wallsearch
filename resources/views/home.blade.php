<!DOCTYPE html>
<html lang="en">
    @include('partials._header')
    <body class="home">
        <div id="wrapper">
            @include('partials._navbar')
            @yield('content')
        </div>
        @include('partials._footer')
    </body>
</html>