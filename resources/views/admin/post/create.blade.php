@extends('admin')
@section('content')
<?php 
    function getRecord($record, $value){
        if(old($value) != null) return old($value);
        return $record->$value;
    }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    @if($record->id)
    Edit Post
    @else
    New Posts
    @endif
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('/badmin/posts') }}"><i class="fa fa-dashboard"></i>Posts</a></li>
    <li class="active">Posts</li>
  </ol>
</section>

<section class="content">
    @include('partials._message')
    @include('partials._error')
    <div class="row">
        <form action="{{ url('badmin/posts/create') }}" method="post" role="form" enctype="multipart/form-data">
            <div class="col-md-9">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>
                    </div>
                    <div class="box-body">
                        <input type="hidden" name="id" value="{{ $record->id }}">
                        <div class="form-group">
                            <label for="">Title</label>
                            <input type="text" class="form-control" name="title" placeholder="title" value="{{ getRecord($record,'title') }}">
                        </div>
                        <div class="form-group">
                            <label for="">Slug</label>
                            <input type="text" class="form-control" name="slug" placeholder="slug" value="{{ getRecord($record,'slug') }}" @if($record->id)disabled @endif>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label for="">Author name</label>
                                    <input type="text" class="form-control" name="author_name" placeholder="author" value="{{ getRecord($record,'author_name') }}">
                                </div>
                                <div class="col-lg-6">
                                    <label for="">Author avatar</label>
                                    <input type="url" class="form-control" name="author_avatar" placeholder="Author avatar image from url" value="{{ getRecord($record,'author_avatar') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Abstract</label>
                            <textarea class="form-control" name="abstract" rows="2">{{ getRecord($record,'abstract') }}</textarea>
                        </div> 
                        <div class="form-group">
                          <label for="pdf_file">Pdf File</label>
                          <input type="file" name="pdf_file" id="pdf_file">
                          <p class="help-block">upload pdf file content @if($record->pdf_file) ({{ $record->pdf_file }}) @endif</p>
                        </div>
                        <div class="form-group">
                            <label for="">Content</label>
                            <textarea class="form-control" name="body" rows="10" id="content_body">{!! getRecord($record,'body') !!}</textarea>
                        </div>        
                    </div>
                </div>
            </div>
            <div class="col-md-3">

                <div class="box">
                        <button class="btn bg-maroon btn-flat margin" name="submit" value="0" type="submit">Draft</button>
                        <button class="btn bg-olive btn-flat" name="submit" value="1">Publish</button>
                </div>
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Created at</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="input-group datepicker">
                              <input type="text" class="form-control" name="created_at" value="{{ getRecord($record,'created_at') }}">
                              <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </span>
                            </div>
                        </div>
                    </div>
                </div>    

                <!--
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! $cats !!}
                        </div>
                    </div>
                </div>  
                -->

                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tags</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <select name="tags[]" class="form-control" multiple>
                                @foreach($tag_list as $list)
                                <option value="{{ $list->id }}" @if(in_array($list->id, $record->tag)) selected @endif>{{ $list->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>          
            </div>
        </form>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function(){

        @if(!$record->id)
        $('input[name="title"]').blur(function(){
           var val = $(this).val().trim(); 
            if(val){
                $('input[name="slug"]').attr('disabled', true); 
                $.get(base_url + '/badmin/createSlug', {text:val}, function(res){
                    $('input[name="slug"]').removeAttr('disabled');
                    $('input[name="slug"]').val(res);
                });
            }
        });
        @else

        /**
        var cat = {!! json_encode($record->cat) !!};
        for (var i = 0; i < cat.length; i++) {
            var val = cat[i]; 
            $('input[name="cats[]"][value="'+ val+'"]').prop('checked', true);
        }
        **/

        @endif

        $('.datepicker').datetimepicker({
            //format:'MM/DD/YYYY HH:mm:ss',
            format:'YYYY-MM-DD HH:mm:ss',
            defaultDate:moment()
        });

        $('select[name="tags[]"]').select2();
        CKEDITOR.replace('content_body');
    });
</script>

@endsection