                          @foreach($list_side as $row)
                          <?php $ext = pathinfo($row->image_url, PATHINFO_EXTENSION); ?>   
                          <li itemscope="" itemprop="associatedMedia" itemtype="http://schema.org/ImageObject">
                              <div class="recent_posts_img">
                                  <meta content="{{ url('assets/fullimage/'. $row->slug . '.' .$ext) }}?content=true" itemprop="contentUrl">
                                  <img itemprop="thumbnail" src="{{ url('assets/images/'. $row->slug . '.' .$ext) }}" alt="{{ $row->title }}" class="p50">
                              </div>
                              <div class="recent_posts_content">
                                  <div class="recent_posts_content_in">
                                      <a class="post_title" href="{{ url($row->slug_category.'/'.$row->slug) }}.html" itemprop="url"><span itemprop="caption">{{ $row->title}}</span></a>
                                      <div class="clear"></div>
                                      <div class="pmeta">
                                          {{ date('F d, Y', strtotime($row->created_at)) }}
                                      </div>
                                      <span class="divider">:</span>
                                      <span class="comments"><a href="#">{{ $row->ratingValue }}</a></span>
                                  </div>
                              </div>
                              <div class="clear"></div>
                          </li>
                          @endforeach