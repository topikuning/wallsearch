@foreach($listing as $row)
	<?php
		$url = url('images/'. $row->slug . '-thumb.jpg');
		if($row->local_image){
			$url = url('/images/thumb/'. $row->thumb_url);
		}
	?>
	<div class="col-sm-4 col-md-3 masonry-item">
		<div class="post-box">
			<a href="{{ url($row->slug.'/'.$row->code) }}">
				<img class="img-responsive" src="{{ $url }}" alt="{{ $row->keyword }}"/>
				<h2>{{ $row->title }}</h2>
			</a>
		</div>
	</div>
@endforeach