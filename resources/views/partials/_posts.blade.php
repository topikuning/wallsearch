@foreach($listing as $row)
	<?php
		$url = url('images/'. $row->slug . '-thumb.jpg');
		if($row->local_image){
			$url = url('/images/thumb/'. $row->thumb_url);
		}
	?>
	<div class="col-md-3 col-sm-4 animate" data-anim-type="zoomIn" data-anim-delay="400">
		<div class="home-gallery-col">
		<div class="home-gallery-img">
			<img src="{{ $url }}" class="img-responsive" alt="{{ $row->keyword.' - '.$row->title }}" />
			<div class="gallery-showcase-overlay">
				<div class="gallery-showcase-overlay-inner">
				<div class="gallery-showcase-icons">
					<a class="photobox_a" href="{{ $url }}" rel="nofollow" title="{{ $row->keyword.' - '.$row->title }}">
						<i class="fa fa-search-plus"></i>
						<img title="{{ $row->keyword.' - '.$row->title }}" alt="{{ $row->keyword.' - '.$row->title }}" src="{{ $url }}" style="display:none;" />
					</a>
					<a href="{{ url($row->slug.'/'.$row->code) }}" title="{{ $row->keyword }}"><i class="fa fa-link"></i></a>
				</div>
				</div>
			</div>
		</div>
		</div>
	</div>
@endforeach